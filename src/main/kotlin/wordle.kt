import java.util.*

/*
    * AUTHOR: Alex Torres
    * TITLE: Wordle
    * DESCRIPTION: Aquest programa es una versió del Wordle.
*/

val yellow = "\u001B[43m"
val green = "\u001B[42m"
val grey = "\u001B[47m"
val reset = "\u001B[0m"
var historial = mutableListOf<MutableList<String>>()
var playagain = true
var letterguess = 0
var positionguess = 0
var paraulausuari: String = "/"
var win = false

/**
 * @author Alex Torres Lemos
 */
fun main(){
    while (playagain) empezarPartida()
}

/**
 * Aquesta funció es amb la que s'inicia el joc.
 * @author Alex Torres Lemos
 * @version 12/01/2023 1.0.0
 */
fun empezarPartida() {

    var intentos = 6
    val scanner = Scanner(System.`in`)

    //VARIABLES COLORES
    println("BIENVENIDO a WORDLE, un juego de palabras donde tienes que pensar para poder ganar...")
    println(" ")
    println("Reglas del juego:")
    println("Tendras que adivinar la palabra secreta, para conseguir eso tendras 6 intentos para poner las palabras que quieras.")
    println("Si coincide una letra de tu palabra con una letra de la palabra secreta esta letra saldra en color $yellow AMARILLO $reset")
    println("Si encima la letra esta en la posicion correcta saldra en  $green VERDE $reset ")
    println("Sin embargo, si la letra no esta en la palabra secreta te saldra de color $grey GRIS $reset ")
    println("RECUERDA: No se puede repetir una letra en la misma palabra.")
    println(" ")
    println("‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒‒")
    println("A JUGAR!!")

    val randomWord = palabraRandom()

    println("Letras posibles a escoger: ")
    println(letrasAbecedario())

    println("Te quedan $intentos intentos")

    for (i in 1 .. intentos) {
        // ??
        println("Introduce una palabra de CINCO letras:")
        val paraulausuari = wordUsuari()
        println("random: $randomWord")
        val historialIntento = compareWord(randomWord, paraulausuari)
        addInHistorial(historialIntento)
        intentos--
        if (randomWord == paraulausuari) {
            win= true
        }

        if (win) break

    }
    resultOfTheGame(win, randomWord)
    askPlayAgain(scanner.toString())

}

/**
 * Aquesta funció guarda en una llista totes les lletres del abecedari.
 * @param lletres Una mutable list amb les lletres del abecedari.
 * @return Retorna totes les lletres del abecedari.
 */
fun letrasAbecedario(): MutableList<Char> {
    //LISTA LETRAS DEL ABECEDARIO
    val letras = mutableListOf<Char>(
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'Ñ',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z'
    )
    return letras
}

/**
 * Aquesta fa que s'esculli una paraula random d'una llista.
 * @param palabra Una llista amb paraules.
 * @param randompalabra Una variable que agafa una paraula random de la llista
 * @return Retorna una paraula random de la llista.
 */
fun palabraRandom():String{
    //ELEGIR PALABRAS RANDOMS
    val palabra = listOf(
        "CORTA", "BATES", "ZANCO", "PINZA", "HABIL", "EDITA", "ABETO", "OLIVA", "MOVIL", "AIREO", "ROSAL", "DISCO", "HUMOR", "RECTO", "LISTA", "CHUTE", "PUNTO", "SALTO", "LIDER", "JUEGO"
    )
    val randompalabra = palabra.random()
    return randompalabra
}

/**
 * Aquesta funció demana una paraula de 5 lletres .
 * @param paraulausuari Un scanner, perque introdueixin una paraula.
 * @return Retorna la paraula introduida per l'usuari.
 */
fun wordUsuari(): String {
    do {
        val scanner = Scanner(System.`in`).useLocale(Locale.UK)
        paraulausuari = scanner.next().uppercase()

    } while (paraulausuari.length != 5)

    return paraulausuari
}

/**
 * Aquesta funció compara la paraula de l'usuari amb la paraula random i pinta les lletres de colors.
 * @param palabraslista Una mutable list amb cada una de les lletres de la paraula introduida per l'usuari.
 * @return Retorna el contingut de la llista..
 */
//COMPARAR LA PALABRA DEL USUARIO CON LA PALABRA SECRETA
fun compareWord(randomWord: String, paraulausuari: String): MutableList<String> {
    val palabraslista = mutableListOf<String>()
    for (i in 0..randomWord.lastIndex) {
        if (paraulausuari[i] == randomWord[i]) {
            palabraslista.add(i, " $green ${paraulausuari[i]} $reset ")
            positionguess++
        } else if (paraulausuari[i] in randomWord) {
            palabraslista.add(i, " $yellow ${paraulausuari[i]} $reset ")
            letterguess++
        } else if (paraulausuari[i] !in randomWord) {
            palabraslista.add(i, " $grey ${paraulausuari[i]} $reset ")
        }
        //BORRAR LAS LETRAS UTILIZADAS
        if (paraulausuari[i] in letrasAbecedario()) {
            letrasAbecedario().remove(paraulausuari[i])
        }
    }
    return palabraslista
}

/**
 * Aquesta funció guarda en una llista la paraula de aquell intent.
 * @return Retorna la paraula que ha introduit junt amb les anteriors.
 */
fun addInHistorial(historialIntento: MutableList<String>) {
    historial.add(historialIntento)
    for (intentoHecho in historial) {
        println(intentoHecho.toString().replace(",", "").replaceFirst("[", "").replace("]", ""))
        println(" ")
    }
}

/**
 * Aquesta funció printa el resultat final de la partida.
 * @return Retorna si has guanyat o no, i quina era la paraula random.
 */
//PRINTAR LAS POSIBLES OPICIONES DEL FINAL DE PARTIDA Y LA LISTA DE LETRAS DEL ABECEDARIO
fun resultOfTheGame(win: Boolean, randomWord: String ){
    if(win) println("FELICIDADES HAS ADIVINADO LA PALABRA!!! \uD83C\uDF89 \uD83C\uDF89 \uD83C\uDF89 ")
    else println("Has perdido!!\uD83D\uDE41, la palabra correcta era ${randomWord}\"")
}

/**
 * Aquesta funció pregunta si vols tornar a jugar.
 * @param turnAgain Un scanner per di si vols jugar un altre cop.
 */
fun askPlayAgain(scanner: String) {
    println("Quieres volver a jugar? Y/N")
    val turnAgain: String = scanner.uppercase()
    if (turnAgain == "N")
        playagain = false
}