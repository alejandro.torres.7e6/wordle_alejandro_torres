import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class WordleKtTest{
    @Test
    fun checkIfPlayAgainWihAnswerY(){
        val expected = "FELICIDADES HAS ADIVINADO LA PALABRA!!! \uD83C\uDF89 \uD83C\uDF89 \uD83C\uDF89 "
        assertEquals(expected, resultOfTheGame(true, "comer" ))
    }

    @Test
    fun checkIfPlayAgainWithAnswerY(){
        val expected = true
        assertEquals(expected, askPlayAgain("Y"))
    }
}
