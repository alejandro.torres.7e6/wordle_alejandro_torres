Contenido
========

 * [Titulo del proyecto](#Titulo)
 * [Descripción del proyecto](#Descripción)
 * [Instalación del proyecto](#Instalación)
 * [Ejecutar el programa](#Ejecucción)
 * [Instrucciones del juego](#Instrucciones)
 * [Want to contribute?](#want-to-contribute)

### Titulo:
---

WORDLE

### Descripción:
---

Este programa es una replica del novedoso juego de Wordle. 
Las reglas de juego y la decoracion del programa són practicamente igual que las del juego original.
El objetivo del juego es conseguir adivinar la palabra secreta en un determinado numero de intentos.

### Instalación:
---

Para instalar el programa tienes que entrar en este link:

```bash
git clone https://gitlab.com/alejandro.torres.7e6/wordle_alejandro.git
```
### Ejecucción:
---

Para ejecutar el programa simplemente tienes que ejecutar el fichero 'wordle.kt'.



### Instrucciones:
---

* La maquina escoge una palabra totalmente aleatoria de 5 letras en las que no sep uede repetir la misma letra en una palabra.
* Tu objetivo es intentar adivinar la palabra secreta aleatoria en menos de 6 intentos.
* Si introduces una palabrsa y hay una letra que esta en la palabra secreta pero no en la misma posicion esa letra se printara en naranja. Si la letra esta en la palabra secreta y encima en la posición correcta se printara en verde.
* Y por ultimo, si la letra no coincide en las dos palabras se printara en gris.
* El juego se acaba cuando adivinas la palabra, eso quiere decir que todas las letras tienen que estar printadas en verde.

### Want to Contribute
---


MIT License

Copyright (c) [2022] [Wordle_Alejandro]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
